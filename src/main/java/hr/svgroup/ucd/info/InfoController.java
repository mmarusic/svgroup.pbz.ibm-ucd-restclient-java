package hr.svgroup.ucd.info;

import hr.svgroup.ucd.common.AppUrls;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class InfoController {

  @Autowired
  private Environment environment;

  @GetMapping(AppUrls.ROOT)
  String infoPage(Model model) {
    String activeProfiles = String.join(",", environment.getActiveProfiles());
    model.addAttribute("activeProfiles", activeProfiles);
    return "index";
  }

}
