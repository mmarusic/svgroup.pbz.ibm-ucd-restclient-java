package hr.svgroup.ucd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication(scanBasePackageClasses = { UcdRestClientApplication.class })
public class UcdRestClientApplication extends SpringBootServletInitializer {

  public static void main(String[] args) {
    SpringApplication.run(UcdRestClientApplication.class, args);
  }

  @Override
  protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
    return application.sources(UcdRestClientApplication.class);
  }

}
